<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="application/xml"
	schematypens="http://purl.oclc.org/dsdl/schematron"?>
<teiCorpus xmlns="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude">
   <teiHeader>
      <fileDesc>
         <titleStmt>
            <title>Édition numérique du manuscrit inédit de la toute première traduction française
               complète du théâtre d’Aristophane par Dom Lobineau</title>
         </titleStmt>
         <publicationStmt>
            <publisher>Litt&amp;Arts (UMR 5316)</publisher>
            <authority>Malika Bastin</authority>
            <authority>ELAN (Équipe Littérature et Arts Numériques) de Litt&amp;Arts</authority>
         </publicationStmt>
         <sourceDesc>
            <p>Information about the source</p>
         </sourceDesc>
      </fileDesc>
      <encodingDesc>
         <projectDesc>
            <p/>
         </projectDesc>
         <editorialDecl>
            <p/>
         </editorialDecl>
      </encodingDesc>
   </teiHeader>
   <TEI xml:id="project">
      <teiHeader>
         <fileDesc>
            <titleStmt>
               <title/>
            </titleStmt>
            <publicationStmt>
               <p/>
            </publicationStmt>
            <sourceDesc>
               <p/>
            </sourceDesc>
         </fileDesc>
      </teiHeader>
      <text>
         <front>
            <div xml:id="home">
               <head>L’Aristophane de Dom Lobineau (1721?)</head>
               <p>Ce site présente l’édition numérique d’un manuscrit inédit de la toute première
                  traduction française complète du théâtre d’Aristophane, composée entre la fin du
                  XVIIe siècle et le début du XVIIIe siècle par l’ecclésiastique et historien Guy
                  Alexis Lobineau (1667-1727), surtout connu pour son <hi rend="italic">Histoire de
                     Bretagne</hi> et sa contribution à l'<hi rend="italic">Histoire de Paris</hi>. </p>

               <p>La première traduction française complète d’Aristophane publiée date de 1784; il
                  s’agit de celle de Poinsinet de Sivry. La traduction de Lobineau fait donc
                  remonter d’environ un siècle la date du premier Aristophane français complet. </p>

               <p> Le manuscrit est conservé à la Médiathèque municipale de Rochefort, sous la cote
                  78-79. C'est un don du Marquis de Queux de Saint-Hilaire. Néo-helléniste et
                  amateur de musique, il possédait un domaine à proximité de Rochefort. Il est
                  probablement à l’origine de la reliure des deux volumes, puisque les autres
                  volumes du fonds Queux de Saint-Hilaire sont reliés de la même façon. </p>
               <p> Les traductions d'Aristophane par Lobineau sont signalées dès 1738 dans le
                  <hi rend='italic'>Journal historique sur les matières du temps</hi>, à propos de ses traductions de
                  Polyen, puis en 1770 par Dom Tassin, qui dans son <hi rend="italic">Histoire
                     littéraire de la congrégation de Saint Maur</hi>, écrit : « Le P. Lobineau a
                  encore traduit plusieurs autres pièces de ce poète comique ; mais ses traductions
                  n’ont point vu le jour </p>

               <p> L'ensemble a été décrit pour la première fois un siècle plus tard par Simon
                  Chardon de la Rochette (1753-1814) dans "Sur Aristophane. Préface du P. Lobineau"
                  publié en 1795 dans le <hi rend="italic">Magasin Encyclopédique</hi> puis repris
                  dans ses <hi rend="italic">Mélanges de critique et de philologie</hi>, volume 3,
                  p. 178-260, en 1812; cette publication contient de larges extraits de
                  l'introduction, dont certains qui sont absents de notre manuscrit. Dans cette
                  publication, on apprend que Chardon de la Rochette a eu accès à deux manuscrits,
                  l'un contenant le texte d’Aristophane édité par Lobineau et l'autre la traduction
                  française des comédies, qui lui ont été, dit-il, communiqués par l’abbé Mercier de
                  Saint Léger en septembre 1792. Seul le manuscrit de la traduction nous est
                  parvenu. Chardon de la Rochette décrit ainsi le manuscrit de la traduction : « Le
                  second manuscrit, partagé en trois volumes in-8°, contient la traduction entière
                  des XI comédies d'Aristophane qui nous restent. Elle est précédée d'une préface
                  judicieuse et piquante, pleine de recherches sur les mœurs des Athéniens, toutes
                  puisées dans Aristophane même. » (p. 179). </p>

               <p> Le manuscrit se présente sous la forme de deux volumes de respectivement 371 et
                  233 feuillets. Le premier volume comprend une préface incomplète puis, dans cet
                  ordre, <hi rend="italic">Les Acarniens, Les Chevaliers, Les Nuées, Les Guêpes, Les
                     femmes à la fête de Cérès. On trouve dans le second La Paix, Les Oiseaux,
                     Lysistrate, Les Grenouilles, L'Assemblée des femmes, Plutus</hi>. </p>

               <p> Il semble que notre manuscrit soit, en ce qui concerne la préface, une copie
                  incomplète du manuscrit qu'a vu Chardon de la Rochette. En effet, dans son
                  manuscrit, la préface "remplit soixante-sept feuillets, signés, au haut de chaque
                  recto, a, b, c, etc. Le troisième alphabet finit à xxx. » tandis qu'elle occupe,
                  dans notre manuscrit, non pas 67 feuillets mais 85; qui plus est, elle n'est pas
                  paginée au recto par des lettres mais en chiffres arabes, de 1 à 85. Enfin,
                  l'écriture est différente de celle des traductions. </p>

               <p> Lobineau n'indique pas quelles éditions d'Aristophane il avait à sa disposition;
                  cependant, il est probable qu'il ait eu accès à celle d'Emile Portet (Genève,
                  1607) ou celle de Jean Ravestein (Leyde, 1670). </p>

               <p>Il a fait l’objet d’une numérisation, puis il a été saisi et encodé en XML-TEI. Le
                  site présente le fac-similé et la transcription qui en a été faite. </p>

               <p>Ce texte présente un intérêt double. Il enrichit considérablement l’histoire des
                  traductions françaises du poète comique, dans la mesure où il constitue un jalon
                  entre les deux premières traductions françaises du poète publiées en 1684, dues à
                  Madame Dacier (<hi rend="italic">Les Nuées</hi> et <hi rend="italic"
                  >Ploutos</hi>), et celle, complète, de Poinsinet de Sivry, parue en 1784. Entre
                  temps, d'autres se sont essayés à traduire Aristophane en français, dont le père
                  Brumoy qui, dans son <hi rend="italic">Théâtre des Grecs</hi> (1730), donnait à
                  lire en français de nombreux extraits du poète comique, mais omettait les passages
                  les plus licencieux. La traduction de Lobineau, elle, est non seulement complète
                  mais très libre - et c'est d'ailleurs peut-être la raison pour laquelle l'homme
                  d'église a décidé de ne pas la publier. Elle constitue en cela un témoignage
                  singulier de la compréhension que l'on pouvait avoir du poète comique quand ne se
                  posait pas la question de la publication - et qu'étaient donc évacués le risque de
                  la censure et les enjeux de réputation. </p>

               <p>Ensuite, ce texte est une source intéressante sur la méthode qu'utilise
                  l'historien Lobineau pour, à partir des textes, rédiger une histoire d'Athènes vue
                  par la lorgnette du poète comique. Dans ses notes en marge il renseigne les <hi
                     rend="italic">realia</hi>, les personnages historiques, tout ce qui a trait à
                  l'histoire d'Athènes mais aussi de la comédie. Ces remarques sont reprises et
                  structurées dans la longue introduction qui ouvre le premier volume, qui comprend
                  quatre sections. La première donne à lire la liste des comédies attribuées à
                  Aristophane et quelques remarques sur l'excellence de ses pièces, notamment parce
                  qu'elles sont représentatives des mœurs des Athéniens. La deuxième porte sur ce
                  qu'Aristophane nous apprend sur la religion, la politique et les coutumes des
                  Athéniens. La troisième section est une prosopographie, et la quatrième comporte
                  quelques remarques sur la traduction. Ainsi voit-on dans l'aller-retour entre la
                  traduction et l'introduction comment Lobineau élabore son Histoire comique
                  d'Athènes, et comment la démarche de l'historien influe sur le processus de la
                  traduction. </p>

               <p>Le projet a été mené grâce à un financement de la MSH-Alpes piloté par Malika
                  Bastin-Hammou. La saisie et l'encodage ont été réalisés par trois stagiaires
                  étudiantes en licence de Lettres classiques à l'Université Grenoble Alpes : Célia
                  Charlois, Diandra Cristache et Joséphine Rambaud. Les stages se sont déroulés au
                  sein de l'UMR 5316 Litt&amp;Arts. </p>

               <p>Elisabeth Greslou a piloté le processus d'encodage, avec l'aide d'Anne Garcia
                  Fernandez. </p>

               <p>Nathalie Arlin s'est chargée de la transformation des données et de la
                  construction du site.</p>
            </div>
            <div xml:id="edito">
               <div>
                  <head>Le manuscrit</head>
                  <p>Le manuscrit publié sur ce site est conservé dans le fonds patrimonial de la
                     Médiathèque municipale de Rochefort, sous la cote 78-79. Il se compose de deux
                     volumes de respectivement 371 et 233 feuillets. Le premier volume comprend une
                     préface incomplète puis, dans cet ordre, <hi rend="italic">Les Acarniens, Les
                        Chevaliers, Les Nuées, Les Guêpes, Les femmes à la fête de Cérès</hi> ; dans
                     le second <hi rend="italic">La Paix, Les Oiseaux, Lysistrate, Les Grenouilles,
                        L'Assemblée des femmes, Plutus</hi>. Les onze pièces, comme l’introduction,
                     présentent des pages avec un corps de texte centré sur la droite, et une marge
                     à gauche. La reliure cache malheureusement la fin des lignes : la lecture du
                     manuscrit se trouve constamment amputée de certains mots, quoique le sens
                     demeure en général compréhensible. À gauche, dans la marge, on lit diverses
                     marques, des notes explicatives à l’encre et des annotations, à l’encre ou au
                     crayon, qui commentent ou corrigent la traduction. La traduction et certaines
                     notes explicatives semblent d’une même main, mais certaines notes sont d’une -
                     ou plusieurs - autres mains. Nous n’avons à ce jour pas pu identifier ces
                     mains.</p>
               </div>
               <div>
                  <head>Le texte grec de la parallélisation</head>
                  <p>Pour étudier la traduction de Lobineau, nous avons fait le choix de faire
                     figurer en parallèle le texte grec d’Aristophane. Cependant, comme nous
                     ignorons à ce jour quelle(s) édition(s) du texte grec Lobineau avait à sa
                     disposition, nous avons décidé de proposer en regard de sa traduction celui de
                     l’édition de F.W. Hall et W.M. Geldart disponible sur le <ref
                        target="http://www.perseus.tufts.edu/hopper/searchresults?q=aristophanes"
                        >site Perseus</ref>.</p>
               </div>
               <div>
                  <head>L’édition numérique</head>
                  <p>L’édition numérique de ce manuscrit s’est déroulée en plusieurs temps. Le texte
                     a d’abord été saisi sur traitement de texte, puis il a été encodé dans un
                     langage XML spécifique au projet ; enfin nous sommes passé en XML-TEI.</p>
               </div>
               <div>
                  <head>Structure de l’édition</head>
                  <p>La structure de l’édition reprend celle du manuscrit. Chaque pièce fait l’objet
                     de fichiers XML séparés et regroupés dans un fichier TEI-Corpus. <ref
                        target="https://gitlab.com/litt-arts-num/tei/raw/master/schema/ELAN_lobineau.rnc"
                        >Le schéma TEI</ref> a été élaboré avec l’outil <ref
                        target="https://roma2.tei-c.org/">Roma</ref>, avec les modules <hi
                        rend="italic">drama</hi> et <hi rend="italic">msdescription</hi>. <lb/>À
                     cela s’ajoutent des structures propres à la TEI et à nos choix de balisage.
                     <lb/>La liste des personnages de la pièce, et la petite préface le cas échéant,
                     sont placées dans la balise <gi>front</gi>, et le reste de la pièce dans
                        <gi>body</gi>. <lb/>Les noms de personnages ont été balisés avec la balise
                        <gi>name</gi>, sauf dans les didascalies initiales ou de prise de parole ;
                     auquel cas, nous avons utilisé les balises <gi>castItem</gi> pour les
                     premières, et <tag>sp</tag> (ainsi que l'attribut <att>who</att> pour les
                     secondes. <lb/>Les dates sont encodées avec <gi>date</gi>, et les noms de lieux
                     avec <tag>name type="place"</tag>. </p>
                  <p>Enfin, un TEI-Header comporte les méta-données du fichier de la pièce, ainsi
                     que le liste exhaustive des noms de personnes ou personnages servant à réaliser
                     l’index. <lb/>Chaque pièce a son index propre.</p>
                  <p>La parallélisation avec le texte grec utilise également un fichier XML pour
                     chaque texte grec (issus du site Perseus), dans lequel ont été ajoutées des
                     balises <gi>anchor</gi> afin de faire correspondre le texte grec et la
                     pagination du manuscrit.</p>
               </div>
               <div>
                  <head>Normalisation</head>
                  <p>En ce qui concerne la normalisation du texte : <list>
                        <item>nous n’avons pas repris les retours à la ligne, et nous n’avons donc
                           pas rendu les tirets de césure ;</item>
                        <item>l’orthographe a été préservée et annotée avec la balise <gi>sic</gi>
                           lorsqu’elle pouvait paraître surprenante ;</item>
                        <item>la graphie a été conservée autant que possible, par exemple les &amp;
                           sont conservés ;</item>
                        <item>la ponctuation a été modernisée, des points devenant des virgules et
                           vice-versa.</item>
                     </list>
                  </p>
               </div>
               <div>
                  <head>Diffusion</head>
                  <p>Le fichier XML de chaque pièce est disponible sur le site, et se télécharge en
                     cliquant sur le logo TEI en haut à droite de la page de la pièce.</p>
               </div>
               <div>
                  <head>Visualisation et fonctionnalités</head>
                  <div>
                     <head rend="h4">Les pièces et l’introduction</head>
                     <p>Le rendu visuel cherche à reproduire le manuscrit. De gauche à droite : le
                        fac-similé de la page du manuscrit numérisée correspondant à la
                        transcription, puis la marge avec les notes et le numéro de la page, et
                        enfin, le corps du texte.</p>
                     <p>Dans le manuscrit, les notes qui sont signalées par une lettre sont à
                        l’encre : elles font partie du travail de commentaire de l’auteur. Les
                        autres ont été ajoutées par la suite, à l’encre ou au crayon : dans tous les
                        cas, une flèche indique ce qui a été utilisé (en gris le crayon, en noir,
                        l’encre) et permet, en cliquant dessus, de retourner à l’endroit visé dans
                        le texte, appel de note ou signe moins formel.</p>
                     <p>Nous avons reproduit les phénomènes suivants : les soulignements, les
                        ratures, et les corrections, toujours rendues dans l’interligne
                        supérieure.</p>
                     <p>Nous avons ajouté des aplats gris pour distinguer les noms de personnages,
                        ainsi qu’un lien pour naviguer entre les occurrences de leur nom dans la
                        pièce et l’index.</p>
                     <p>Les noms de lieux sont signalés par un aplat jaune pâle.</p>
                     <p>Le gris foncé, souligné et avec le signe <code>[?]</code> indique les
                        reconstitutions des passages illisibles soit à cause de la graphie, soit à
                        cause de la reliure.</p>
                  </div>

                  <div>
                     <head rend="h4">Les index</head>
                     <p>Les index, propres à chaque pièce, unifient toutes les occurrences balisées
                        d’un personnage grâce à l’attribut <att>xml:id</att>, et en donnent le
                        compte. Ils sont classés par des catégories (<att>role</att>) déterminées
                        par la prosopographie qui clôt l’introduction de Dom Lobineau, et complétées
                        par les indications de l’édition critique d’Aristophane d’Alan H.
                        Sommerstein (The comedies of Aristophanes, 11 vol., Warminster, Aris &amp;
                        Phillips, 1980-2001). En cliquant sur un nom, on revient à la pièce avec un
                        outil permettant de naviguer entre les différentes occurrences.</p>
                  </div>

                  <div>
                     <head rend="h4">La parallélisation</head>
                     <p>La parallélisation met en regard les pièces telles que nous les avons
                        transcrites, et le texte grec issu du site Perseus. Ce dernier a été
                        segmenté pour correspondre aux pages de la traduction française de Dom
                        Lobineau.</p>
                  </div>
               </div>
            </div>
         </front>
         <body>
            <p/>
         </body>
      </text>
   </TEI>
   <xi:include href="introduction.xml"/>
   <xi:include href="acarniens.xml"/>
   <xi:include href="chevaliers.xml"/>
   <xi:include href="nuees.xml"/>
   <xi:include href="guepes.xml"/>
   <xi:include href="thesmophories.xml"/>
   <xi:include href="paix.xml"/>
   <xi:include href="oiseaux.xml"/>
   <xi:include href="lysistrate.xml"/>
   <xi:include href="assemblee.xml"/>
   <xi:include href="grenouilles.xml"/>
   <xi:include href="ploutos.xml"/>
</teiCorpus>

var modaleHandler = {
  createModale: function(id, title, content, header) {
    if (document.getElementById(id) === null) {
      var html = '<div tabindex="-1" role="dialog" class="modal" id="' + id + '">';
      html += '<div class="modal-dialog modal-lg">';
      html += '<div class="modal-content">';
      if (header) {
        html += '<div class="modal-header">';
        html += '<h1 class="modal-title">' + title +'</h1>';
        html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        html += '</div>';
      }
      html += '<div class="modal-body">';
      html += '</div>';
      html += '</div>';
      html += '</div>';
      html += '</div>';

      $("body").append(html);
    }

    $("#" + id).find(".modal-body").html(content);
    $('#' + id).modal('show');

    return;
  },

  createImageModale: function(idImage) {
    var img = '<img style="width:100%" src="facs/light/'+ idImage+'"/>';
    this.createModale('modale-image', idImage, img, false);

    return;
  }
}

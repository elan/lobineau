$(window).on("load", function() {
    navigate.init(true)
});

$(document).on('click','span.personnage > a',function(event){
    event.preventDefault();
    event.stopPropagation();

    window.location.hash = '#'+$(this).data("id");
    navigate.init(false);
});

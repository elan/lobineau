var navigate = {
  selection: null,

  init: function(scroll) {
    var pers = window.location.href.split('#')[1];
    var selection = $("a[data-id=" + pers + "]");
    if (selection.length > 0) {
      if (this.selection) this.selection.removeClass("surligne");
      selection.addClass("surligne");
      this.selection = selection;
      this.length = selection.length;
      this.position = 0;
      this.updateContent();
      $("#nav-box").show();
      if (scroll) this.scrollTo();
    }

    return this.selection;
  },

  next: function() {
    this.position = (this.position >= this.length - 1) ?
      0 :
      this.position + 1;

    this.scrollTo();

    return;
  },

  previous: function() {
    this.position = (this.position == 0) ?
      this.length - 1 :
      this.position - 1;

    this.scrollTo();

    return;
  },

  scrollTo: function() {
    this.selection[this.position].scrollIntoView();
    // add an offset.
    var scrolledY = window.scrollY;
    window.scroll(0, scrolledY - 120);
    this.updateContent();

    return;
  },

  updateContent: function() {
    var index = this.position;
    var play = new URL(location.href).searchParams.get('play');
    $("#nav-length").html(this.length);
    $("#nav-title").html(this.selection[index].textContent);
    $("#nav-position").html(index + 1);
    $("#nav-index").attr("href", "?page=person_"+play+"&play="+play+$(this.selection[index]).attr("href"));

    return;
  }
}

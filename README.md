# Lobineau
Ce site présente l’édition numérique d’un manuscrit inédit de la toute première traduction française complète du théâtre d’Aristophane, composée entre la fin du XVIIe siècle et le début du XVIIIe siècle par l’ecclésiastique et historien Guy Alexis Lobineau (1667-1727), surtout connu pour son Histoire de Bretagne et son Histoire de Paris.

## Prérequis
* git
* php
* composer

## Installation
```
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/elan/lobineau.git
cd lobineau
composer install
cp auth/passwords.php.dist auth/passwords.php
// edit auth/passwords.php
```

## Mise à jour
```
git pull origin master
composer install
```

## Livrer une nouvelle pièce sur le site
1. Pré-requis
  * Fichier d'entrée : ``piece.xml`` (au format XML/TEI)
  * Fichiers nécessaires : les deux XSLT ``tei2html_v20171203.xsl`` et ``tei2index_person_v20171203``
  * Logiciel utilisé : ``Oxygen Editor``
  * \[optionnel] Script : ``generate_lights_and_thumbs.bash``  
1. Valider le TEI et le compléter le cas échéant
  * ouvrir sous Oxygen et vérifier la syntaxe, les erreurs, etc.
  * renseigner si ce n'est pas le cas, au minimum, dans ``/TEI/teiHeader/fileDesc/titleStmt/title`` : ``@type = 'ouvrage'`` et valeur : "La pièce" (par exemple, "Les Acarniens")
1. Générer les html de sortie
  * Sous Oxygen, créer les 2 scénarios de transformation pour la nouvelle pièce :
   * copier-coller deux scénarios d'une autre pièce et adapter en remplaçant les noms de fichier en entrée (le XML) et sortie (le HTML) ainsi  que le nom du scénario
   * les 2 scénarios sont :
     * ``XXX - tei2index_person`` : créé la page HTML de la pièce
     * ``XXX - tei2html`` 	      : créé la page HTML de l'index des personnes
  * Appliquer les 2 scénarios et vérifier qu'Oxygen n'indique pas d'erreur
1. Créer les pages templates twig pour la nouvelle pièce
  * Copier par exemple "``acarniens.html.twig``" "``person_acarniens.html.twig``" vers "``XXX.html.twig" et "person_XXX.html.twig``"
  * Adapter dans chacun de ces fichiers le nom de la pièce (titre + include)
1. Mettre à jour les templates
  * Mettre à jour le menu principal du site en activant ou en complétant les sous-menus nécessaires dans le fichier view/base.html.twig
   * activer dans le menu "Les pièces" : ``<li class=" disabled"><a href="#" title="en construction">Les Acarniens</a></li>`` devient ``<li class="leaf"><a href="index.php?page=acarniens">Les Acarniens</a></li>`` ET compléter : ``{% set indexes = ["chevaliers"] %}``
   * ajouter le lien vers l'index des personnages dans le menu "Index généraux" : ``<li class="first leaf"><a href="index.php?page=person_acarniens">Personnages dans les Acarniens</a></li>`` ET compléter : ``{% set indexes = ["person_chevaliers", "person_introduction", "person_guepes", "person_acarniens"] %}``
1. \[si ce n'est pas le cas] Générer les vignettes et images réduites des facsimilés
  * pour toutes les images déclarées dans /TEI/facsimile :
   * créer les vignettes (142x222) et les mettre dans le dossier ``facs/thumbnails/``
   * créer les images réduites (595x934) et les mettre dans le dossier ``facs/light/``
   * sous Linux, utiliser le script ``generate_lights_and_thumbs.bash``
     * ex. ``generate_lights_and_thumbs.bash ../tei/acarniens.xml p_Lobineau/Images_Lobineau/Lobineau_images78_coupées/ ../facs/ 0``
   * (éventuellement) traiter à la main les erreurs (fichier manquant ou autre)
1. Vérifier les choses sur son serveur local
  * Donc avoir un serveur local et fait toute l'installation pour "bien" visualiser les choses
1. Transférer sur le serveur
 * Giter son travail
 * Depuis le serveur du site (Stendhal actuellement), récupérer les nouveautés du git
 * Vérifier le résultat

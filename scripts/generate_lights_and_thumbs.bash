#!/bin/bash

if [[ $# -ne 4 ]]
then
    echo "Ce script attend 4 arguments en entrée, dans l'ordre suivant :"
    echo "  - le chemin vers le fichier XML/TEI à traiter"
    echo "  - le chemin vers le dossier contenant les facsimilés"
    echo "  - le dossier de sortie des images, contenant 2 sous-dossiers : light/ et thumbnails/"
    echo "  - 1 s'il faut écraser les éventuels fichiers de sortie déjà existant ; 0 sinon"
    echo
    echo "Exemple :"
    echo "  generate_lights_and_thumbs.bash ../tei/acarniens.xml /home/annegf/Data/Projets/ITHAC/p_Lobineau/Images_Lobineau/Lobineau_images78_coupées/ ../facs/ 0"
    exit
fi


input_xml=$1
dir_facs=`echo $2 | sed 's#/$##'`
output_dir=`echo $3 | sed 's#/$##'`
overwrite=$4

if [[ ! -d $output_dir && ! -d "$output_dir/light" && ! -d "$output_dir/thumbnails" ]]
then
    echo "Le dossier de sortie n'existe pas ou bien ne contient pas 2 sous-dossiers : light/ et thumbnails/"
    exit
fi

cat $input_xml | sed '1,/<facsimile>/ d ; /<\/facsimile/,$ d' | while read line
do
    file=`echo $line | sed 's#.*<surface xml:id=.##; s#./>.*$##'`
    file=`echo $file | sed 's#.*<graphic url=.##; s#.>.*$##'`
    echo -e $file | awk '{ printf("%.25s ", $1); }'
    input_file=$dir_facs"/"$file
    if [[ $overwrite -eq 1 || ! -f "$output_dir/light/$file" || ! -f "$output_dir/thumbnails/$file" ]]
    then
	if [[ ! -f $input_file ]]
	then
	    name=`filename $input_xml | sed 's#.xml##; s/^./\u&/'`
	    new_input_file=$dirname`echo "$input_file" | sed -e 's#\(_[ab].jpg\)#-'$name'\1#'`
	    if [[ ! -f $new_input_file ]]
	    then
		echo "# [$file] Fichier source manquant" >&2
		echo "# Utiliser les commandes suivantes en remplaçant le nom du fichier source :"
		echo "convert -resize 595x934 \"$dir_facs/$file\" \"$output_dir/light/$file\""
		echo "convert -resize 142x222 \"$dir_facs/$file\" \"$output_dir/thumbnails/$file\""
	    else
		input_file=$new_input_file
	    fi
	fi
	if [[ -f $input_file ]]
	then
	    if [[ $overwrite -eq 1 || ! -f "$output_dir/light/$file" ]]
	    then
		convert -resize 595x934 "$input_file" "$output_dir/light/$file"
		echo -e "$input_file \t light \t DONE\r\c"
	    fi
	    if [[ $overwrite -eq 1 || ! -f "$output_dir/thumbnails/$file" ]]
	    then
		convert -resize 142x222 "$input_file" "$output_dir/thumbnails/$file"
		echo -e "$input_file \t thumb \t DONE\r\c"
	    fi
	fi
    fi
done
echo

<?php

include("./auth/auth.php");

$page = (isset($_GET["page"])) ? $_GET["page"] : "index" ;
$twig = loadTwig();

$html = file_get_contents('views/html/'.$page.'.html');
if (preg_match('/^person_/', $page)) {
    echo $twig->render('person.html.twig', ['page' => $page, 'html' => $html]);
} else {
    echo $twig->render('default.html.twig', ['page' => $page, 'html' => $html]);
}

function loadTwig()
{
    require_once 'vendor/autoload.php';
    $loader = new Twig_Loader_Filesystem('views');
    $twig = new Twig_Environment($loader, array());

    return $twig;
}

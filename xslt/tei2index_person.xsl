<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs" version="2.0" xpath-default-namespace="http://www.tei-c.org/ns/1.0">

    <xsl:template match="tei:TEI" mode="person">
        <xsl:param name="play"/>
        <xsl:variable name="filename">
            <xsl:value-of select="$play"/>
            <xsl:text>.xml</xsl:text>
        </xsl:variable>

        <xsl:variable name="persons">
            <personnes>
                <xsl:for-each-group select=".//name/@ref" group-by=".">
                    <xsl:sort select="."/>
                    <xsl:variable name="id">
                        <xsl:value-of select="substring-after(., '#')"/>
                    </xsl:variable>
                    <xsl:if test="not(//*[@xml:id = $id]/parent::tei:respStmt)">
                        <xsl:choose>
                            <xsl:when test="//tei:TEI[@xml:id = $play]//person[@xml:id = $id]">
                                <personne id="{$id}" count="{count(current-group())}">
                                    <xsl:for-each
                                        select="//tei:TEI[@xml:id = $play]//person[@xml:id = $id]/tokenize(@role, ' ')">
                                        <role>
                                            <xsl:attribute name="id">
                                                <xsl:value-of select="."/>
                                            </xsl:attribute>
                                            <xsl:value-of select="replace(., '_', ' ')"/>
                                        </role>
                                    </xsl:for-each>
                                    <name>
                                        <xsl:value-of
                                            select="//tei:TEI[@xml:id = $play]//person[@xml:id = $id]/persName[1]"
                                        />
                                    </name>
                                </personne>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:message>WARNING: name @id "<xsl:value-of select="$id"/>" used
                                    but not declared in the TEI header in file <xsl:value-of
                                        select="$filename"/>.</xsl:message>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:if>
                </xsl:for-each-group>
            </personnes>
        </xsl:variable>

        <H2 class="center">Index des personnages dans <i><xsl:value-of
                    select=".//title[@type = 'ouvrage']"/></i></H2>

        <ul class="nav nav-tabs nav-fill" id="roleTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="role-tab" data-toggle="tab" href="#role" role="tab"
                    aria-controls="role" aria-selected="true">Par rôle</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="alpha-tab" data-toggle="tab" href="#alpha" role="tab"
                    aria-controls="alpha" aria-selected="false">Par ordre alphabétique</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="frequency-tab" data-toggle="tab" href="#frequency"
                    role="tab" aria-controls="frequency" aria-selected="false">Par fréquence dans la
                    pièce</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="role" role="tabpanel"
                aria-labelledby="role-tab">
                <xsl:apply-templates mode="by_role" select=".">
                    <xsl:with-param name="play" select="$play"/>
                    <xsl:with-param name="persons" select="$persons"/>
                </xsl:apply-templates>
            </div>
            <div class="tab-pane fade" id="alpha" role="tabpanel" aria-labelledby="alpha-tab">
                <xsl:apply-templates mode="by_person" select=".">
                    <xsl:with-param name="play" select="$play"/>
                    <xsl:with-param name="persons" select="$persons"/>
                </xsl:apply-templates>
            </div>
            <div class="tab-pane fade" id="frequency" role="tabpanel"
                aria-labelledby="frequency-tab">
                <xsl:apply-templates mode="by_frequency " select=".">
                    <xsl:with-param name="play" select="$play"/>
                    <xsl:with-param name="persons" select="$persons"/>
                </xsl:apply-templates>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="teiHeader" mode="#all"/>
    <!--on ne veut pas le contenu du header-->

    <xsl:template match="front" mode="#all"/>
    <!--on ne veut pas le contenu du front-->

    <xsl:template match="text" mode="by_frequency">
        <xsl:param name="play" required="yes"/>
        <xsl:param name="persons" required="yes"/>
        <xsl:variable name="filename">
            <xsl:value-of select="$play"/>
            <xsl:text>.xml</xsl:text>
        </xsl:variable>
        <div>
            <ul style="column-count: 3;">
                <xsl:for-each-group select="$persons//*:personne" group-by="@count">
                    <xsl:sort select="current-grouping-key()" data-type="number" order="descending"/>
                    <xsl:for-each select="$persons//*:personne[@count = current-grouping-key()]">
                        <xsl:sort select="./*:name[1]"/>
                        <li>
                            <a class="person">
                                <xsl:attribute name="href">
                                    <xsl:text>index.php?page=</xsl:text>
                                    <xsl:value-of select="substring-before($filename, '.xml')"/>
                                    <!-- nom de fichier sans l'extension -->
                                    <xsl:text>&amp;play=</xsl:text>
                                    <xsl:value-of select="substring-before($filename, '.xml')"/>
                                    <xsl:text>#</xsl:text>
                                    <xsl:value-of select="@id"/>
                                </xsl:attribute>
                                <xsl:attribute name="data-id">
                                    <xsl:text>pers_</xsl:text>
                                    <xsl:value-of select="@id"/>
                                </xsl:attribute>
                                <xsl:if test="string-length(*:name) = 0">
                                    <i>no name</i>
                                </xsl:if>
                                <xsl:value-of select="*:name"/>
                            </a>
                            <xsl:text> (</xsl:text>
                            <xsl:value-of select="@count"/>
                            <xsl:text>) : </xsl:text>
                            <xsl:for-each select="*:role">
                                <xsl:value-of select="."/>
                                <xsl:if test="position() != last()">
                                    <xsl:text>, </xsl:text>
                                </xsl:if>
                            </xsl:for-each>
                            <xsl:text>.</xsl:text>
                        </li>
                    </xsl:for-each>
                </xsl:for-each-group>
            </ul>
        </div>
    </xsl:template>

    <xsl:template match="text" mode="by_person">
        <xsl:param name="play" required="yes"/>
        <xsl:param name="persons" required="yes"/>
        <xsl:variable name="filename">
            <xsl:value-of select="$play"/>
            <xsl:text>.xml</xsl:text>
        </xsl:variable>
        <!--<xsl:value-of select="count($persons//*:personne)"/>-->
        <div>
            <ul style="column-count: 3;">
                <xsl:for-each-group select="$persons//*:personne" group-by=".//*:name">
                    <xsl:sort select="current-grouping-key()"/>
                    <li>
                        <a class="person">
                            <xsl:attribute name="href">
                                <xsl:text>index.php?page=</xsl:text>
                                <xsl:value-of select="substring-before($filename, '.xml')"/>
                                <!-- nom de fichier sans l'extension -->
                                <xsl:text>&amp;play=</xsl:text>
                                <xsl:value-of select="substring-before($filename, '.xml')"/>
                                <xsl:text>#</xsl:text>
                                <xsl:value-of select="@id"/>
                            </xsl:attribute>
                            <xsl:attribute name="data-id">
                                <xsl:text>pers_</xsl:text>
                                <xsl:value-of select="@id"/>
                            </xsl:attribute>
                            <xsl:if test="string-length(*:name) = 0">
                                <i>no name</i>
                            </xsl:if>
                            <xsl:value-of select="*:name"/>
                        </a>
                        <xsl:text> (</xsl:text>
                        <xsl:value-of select="@count"/>
                        <xsl:text>) : </xsl:text>
                        <xsl:for-each select="*:role">
                            <xsl:value-of select="."/>
                            <xsl:if test="position() != last()">
                                <xsl:text>, </xsl:text>
                            </xsl:if>
                        </xsl:for-each>
                        <xsl:text>.</xsl:text>
                    </li>
                </xsl:for-each-group>
            </ul>
        </div>
    </xsl:template>

    <xsl:template match="text" mode="by_role">
        <xsl:param name="play" required="yes"/>
        <xsl:param name="persons" required="yes"/>
        <xsl:variable name="filename">
            <xsl:value-of select="$play"/>
            <xsl:text>.xml</xsl:text>
        </xsl:variable>
        <div class="select-role">
            <xsl:for-each-group select="$persons//*:role" group-by=".">
                <xsl:sort select="."/>
                <a role="button" class="btn btn-light btn-sm" style="font-variant: small-caps;">
                    <xsl:attribute name="href">
                        <xsl:text>#</xsl:text>
                        <xsl:value-of select="@id"/>
                    </xsl:attribute>
                    <xsl:value-of select="."/>
                </a>
            </xsl:for-each-group>
        </div>

        <div class="card-columns">
            <xsl:for-each-group select="$persons//*:personne" group-by="./*:role">
                <xsl:sort select="normalize-space(lower-case(.))"/>
                <div class="role card text-center">
                    <h5 class="card-header" style="font-variant: small-caps;">
                        <!--:first-letter{ text-transform: capitalize } -->
                        <xsl:attribute name="id">
                            <xsl:value-of select="./*:role/@id"/>
                        </xsl:attribute>
                        <xsl:value-of select="current-grouping-key()"/>
                    </h5>
                    <div class="card-body">
                        <ul class="listperson card-text">
                            <xsl:for-each select="current-group()">
                                <xsl:variable name="xmlid" select="@id"/>
                                <xsl:variable name="count" select="@count"/>
                                <xsl:variable name="name" select="./*:name"/>
                                <li>
                                    <a class="person">
                                        <xsl:attribute name="href">
                                            <xsl:text>index.php?page=</xsl:text>
                                            <xsl:value-of
                                                select="substring-before($filename, '.xml')"/>
                                            <!-- nom de fichier sans l'extension -->
                                            <xsl:text>&amp;play=</xsl:text>
                                            <xsl:value-of
                                                select="substring-before($filename, '.xml')"/>
                                            <xsl:text>#</xsl:text>
                                            <xsl:value-of select="$xmlid"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="data-id">
                                            <xsl:text>pers_</xsl:text>
                                            <xsl:value-of select="$xmlid"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="$name"/>
                                    </a>
                                    <xsl:text> (</xsl:text>
                                    <xsl:value-of select="$count"/>
                                    <xsl:text>)</xsl:text>
                                </li>
                            </xsl:for-each>
                        </ul>
                    </div>
                </div>
            </xsl:for-each-group>
        </div>
    </xsl:template>
</xsl:stylesheet>

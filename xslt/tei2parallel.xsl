<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mon-document [
    <!ENTITY times "&#215;">
    <!ENTITY carriage_return "&#8617;" >
    <!ENTITY non_breakable_space "&#160;" >
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="2.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">

    <!--<xsl:output method="xhtml" omit-xml-declaration="yes" indent="yes"/>-->
    <xsl:strip-space elements="*"/>


    <xsl:variable name="default_debug_mode">0</xsl:variable>
    <xsl:variable name="default_view_editor_notes">1</xsl:variable>

    <xsl:key name="txt-by-pb" match="node()"
        use="generate-id(preceding-sibling::pb[position() = 1])"/>

    <xsl:key name="txt-by-anchor" match="node()"
        use="generate-id(preceding-sibling::anchor[position() = 1])"/>

    <xsl:template match="tei:TEI" mode="parallel">
        <xsl:param name="play"/>
        <xsl:param name="view_editor_notes" select="$default_view_editor_notes"/>
        <xsl:param name="debug" select="$default_debug_mode"/>
        <xsl:variable name="filename">
            <xsl:value-of select="$play"/>
            <xsl:text>.xml</xsl:text>
        </xsl:variable>
        <xsl:variable name="greek_filename">
            <xsl:text>byPage/greek/?select=greek_</xsl:text>
            <xsl:value-of select="$filename"/>
            <xsl:text/>
        </xsl:variable>
        <xsl:variable name="greek_file" select="collection($greek_filename)"/>
        <style about="to_move_when_ready">
            .greek {
                background: rgb(245, 245, 245);
            }
            .row .col.greek {
                margin-bottom: 15px;
                border-bottom: 1px solid lightgrey;
            }
            .info {
                font-style: italic;
                color: grey;
                text-align: center;
                display: block;
            }
            .thumbnail.small {
                width: 35px;
                height: 55px;
            }
            .line {
                display: block;
                padding-left: 2em;
            }</style>
        <xsl:if test="$debug = 1">
            <style>
                .row {
                    border: red 1px solid;
                }
                .col {
                    border: green 1px solid;
                }
                .clearfix::before {
                    content: "";
                    clear: both;
                    display: table;
                }
                .imagette {
                    background: yellow;
                }
                .notes {
                    background: orange;
                }
                .transcription {
                    background: lightgrey;
                }
                .greek {
                    background: rgb(250, 250, 190);
                }</style>
        </xsl:if>
        <xsl:if test="$default_view_editor_notes = 1">
            <style>
                
            </style>
        </xsl:if>

        <div class="row content">
            <div class="col col-12">
                <div class="row top_banner">
                    <div class="col col-2" style="font-size:small; color:lightgrey;">
                        <i>Mise à jour : <xsl:value-of
                                select="format-date(current-date(), '[D01]/[M01]/[Y0001]')"/></i>
                    </div>
                    <div class="col col-1 offset-9" style="display:none;">
                        <xsl:element name="a">
                            <xsl:attribute name="href">tei/<xsl:value-of select="$filename"
                                /></xsl:attribute>
                            <xsl:attribute name="id">tei_link</xsl:attribute>
                            <xsl:attribute name="download"/>
                            <img src="img/logo_TEI.svg" title="Voir la source TEI"
                                alt="Voir la source TEI" width="30px"
                                style="vertical-align: middle;"/>
                        </xsl:element>
                    </div>
                </div>
                <xsl:for-each select=".//tei:pb">
                    <xsl:variable name="this_pb"><xsl:value-of select="following-sibling::tei:fw[1]"/></xsl:variable>
                    <div class="row">
                        <div class="col col-2 order-2 notes_and_info">
                            <xsl:apply-templates select="." mode="small"/>
                            <xsl:for-each select="key('txt-by-pb', generate-id())">
                                <xsl:if test="./ancestor::tei:TEI[@xml:id = $play]">
                                    <xsl:apply-templates select="fw[@type = 'pageNum']" mode="small"/>
                                    <xsl:if test="name(.) = 'fw' and @type = 'pageNum'">
                                        <xsl:apply-templates select="." mode="small"/>
                                    </xsl:if>
                                </xsl:if>
                            </xsl:for-each>
                            <xsl:for-each select="key('txt-by-pb', generate-id())">
                                <xsl:if test="./ancestor::tei:TEI[@xml:id = $play]">
                                    <xsl:apply-templates select="note" mode="do"/>
                                    <xsl:if test="name(.) = 'note'">
                                        <xsl:apply-templates select="." mode="do"/>
                                    </xsl:if>
                                    <xsl:if
                                        test="//tei:TEI[@xml:id = $play]//tei:front/descendant::tei:pb[last()]/following-sibling::tei:fw[1] = $this_pb and position() = last()">
                                    <xsl:for-each
                                        select="//tei:TEI[@xml:id = $play]//tei:body//tei:pb[1]/preceding::tei:note[exists(ancestor::tei:body)]">
                                        <xsl:if test="./ancestor::tei:TEI[@xml:id = $play]">
                                            <xsl:apply-templates select="." mode="do"/>
                                        </xsl:if>
                                    </xsl:for-each>
                                </xsl:if>
                                </xsl:if>
                            </xsl:for-each>
                        </div>
                        <div class="col col-5 order-3 transcription">
                            <xsl:for-each select="key('txt-by-pb', generate-id())">
                                <xsl:if test="./ancestor::tei:TEI[@xml:id = $play]">
                                    <xsl:apply-templates select="."/>
                                    <xsl:if
                                        test="//tei:TEI[@xml:id = $play]//tei:front/descendant::tei:pb[last()]/following-sibling::tei:fw[1] = $this_pb and position() = last()">
                                    <xsl:for-each
                                        select="//tei:TEI[@xml:id = $play]//tei:body//tei:pb[1]/preceding::*[exists(ancestor::tei:body) and name() != 'note' and name() != 'ref' and name() != 'name' and name() != 'seg' and name() != 'div'] except //tei:body//tei:pb[1]/preceding-sibling::*">
                                        <xsl:if test="./ancestor::tei:TEI[@xml:id = $play]">
                                            <xsl:apply-templates select=". except .[parent::tei:note]"/>
                                        </xsl:if>
                                    </xsl:for-each>
                                </xsl:if>
                                </xsl:if>
                            </xsl:for-each>
                        </div>
                        <div class="col col-5 order-4 greek">
                            <xsl:apply-templates select="$greek_file" mode="greek">
                                <xsl:with-param name="facs">
                                    <xsl:value-of select="substring-after(@facs, '#')"/>
                                </xsl:with-param>
                                <xsl:with-param name="filename">
                                    <xsl:value-of select="$filename"/>
                                </xsl:with-param>
                            </xsl:apply-templates>
                        </div>
                    </div>
                </xsl:for-each>
            </div>
        </div>
    </xsl:template>

    <xsl:template mode="greek" match="/">
        <xsl:param name="facs" required="yes"/>
        <xsl:param name="filename"/>
        <xsl:if test="count(//anchor[@xml:id = $facs]) = 0">
            <span class="info">
                <xsl:text>Pas de texte original pour cette page</xsl:text>
                <!--<xsl:text> (</xsl:text>
                <xsl:value-of select="$facs"/>
                <xsl:text>)</xsl:text>-->
            </span>
        </xsl:if>
        <xsl:if test="count(//anchor[@xml:id = $facs]) > 1">
            <span class="info">
                <xsl:text>Plus d'un répère utilisé pour cette page (</xsl:text>
                <xsl:value-of select="$facs"/>
                <xsl:text>)</xsl:text>
            </span>
        </xsl:if>
        <xsl:for-each select="//anchor[@xml:id = $facs]">
            <!--<xsl:call-template name="anchor_list">
                <xsl:with-param name="file">
                    <xsl:value-of select="$filename"/>
                </xsl:with-param>
            </xsl:call-template>-->
            <xsl:for-each select="key('txt-by-anchor', generate-id())">
                <xsl:apply-templates select="."/>
            </xsl:for-each>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="l">
        <span class="line">
            <xsl:if test="@n and number(replace(@n,'/[a-z]$/','')) mod 5 = 0">
                <xsl:attribute name="n" select="@n"/>
            </xsl:if>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="said">
        <xsl:if test=" count(preceding::said) = 0 or @who != preceding::said[1]/@who">
            <span class="speaker">
                <xsl:value-of select="substring-after(@who, '#')"/>
            </span>
        </xsl:if>
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="listPerson">
        <ul>
            <xsl:for-each select="person">
                <li class="actor">
                    <span class="acteur">
                        <xsl:apply-templates/>
                    </span>
                </li>
            </xsl:for-each>
        </ul>
    </xsl:template>

    <xsl:template match="pb" mode="small">
        <xsl:variable name="facs" select="substring-after(@facs, '#')"/>
        <div class="imagette">
            <span onclick='modaleHandler.createImageModale("{$facs}")'>
                <xsl:element name="img">
                    <xsl:attribute name="class">thumbnail small</xsl:attribute>
                    <xsl:attribute name="title">voir la page</xsl:attribute>
                    <xsl:attribute name="src">facs/thumbnails/<xsl:value-of select="$facs"
                        /></xsl:attribute>
                    <xsl:attribute name="alt">fac-similé <xsl:value-of select="$facs"
                        /></xsl:attribute>
                </xsl:element>
            </span>
        </div>
    </xsl:template>

    <xsl:template match="fw[@type = 'pageNum']" mode="small">
        <p class="pagenum small">
            <xsl:if test="@rend">
                <xsl:attribute name="data-rend">
                    <xsl:value-of select="@rend"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    
    <!--<xsl:template match="anchor" name="anchor_list" mode="greek">
        <xsl:param name="file"/>
        <xsl:message>
            <xsl:value-of select="$file"/>
            <xsl:text>:</xsl:text>
            <xsl:value-of select="@xml:id"/>
        </xsl:message>
    </xsl:template>-->
</xsl:stylesheet>

<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
  xmlns:tei="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="tei"
  xmlns="http://www.tei-c.org/ns/1.0">
  
  <xsl:output encoding="utf-8" indent="no" method="xml"/>
  
  <xsl:template match="* | @*" mode="splitByAnchor">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="tei:front" mode="splitByAnchor">
    <xsl:variable name="context" select="." as="element(tei:front)"/>
    <xsl:copy>
      <xsl:for-each-group select="descendant::node()[not(node())]" group-starting-with="tei:anchor">
        <xsl:if test="position() > 1">
          <div>
            <xsl:attribute name="type">page</xsl:attribute>
            <xsl:copy-of select="self::tei:anchor"/>
            <xsl:apply-templates select="$context/*" mode="split">
              <xsl:with-param name="restricted-to" select="current-group()/ancestor-or-self::node()"
                tunnel="yes"/>
            </xsl:apply-templates>
          </div>
        </xsl:if>
      </xsl:for-each-group>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="tei:body" mode="splitByAnchor">
    <xsl:variable name="context" select="." as="element(tei:body)"/>
    <xsl:copy>
      <xsl:for-each-group select="descendant::node()[not(node())]" group-starting-with="tei:anchor">
        <xsl:if test="position() > 1">
          <div>
            <xsl:attribute name="type">page</xsl:attribute>
            <xsl:copy-of select="self::tei:anchor"/>
            <xsl:apply-templates select="$context/*" mode="split">
              <xsl:with-param name="restricted-to" select="current-group()/ancestor-or-self::node()"
                tunnel="yes"/>
            </xsl:apply-templates>
          </div>
        </xsl:if>
      </xsl:for-each-group>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="node()" mode="split">
    <xsl:param name="restricted-to" as="node()+" tunnel="yes"/>
    <xsl:if test="exists(. intersect $restricted-to)">
      <xsl:copy>
        <xsl:copy-of select="@*"/>
        <xsl:apply-templates mode="#current"/>
      </xsl:copy>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="tei:anchor" mode="split"/>
  
</xsl:stylesheet>

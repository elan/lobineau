<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="xs tei"
    version="2.0">
    <xsl:import href="tei2view.xsl"/>
    <xsl:import href="tei2index_person.xsl"/>
    <xsl:import href="tei2parallel.xsl"/>
    <xsl:import href="splitByAnchors.xsl"/>
    
    <!--<xsl:strip-space elements="*"/>-->

    <xsl:output method="text" encoding="UTF-8" omit-xml-declaration="yes"/>

    <xsl:variable name="files" select="collection('../tei/?select=*.xml')"/>
    <xsl:variable name="newline">
        <xsl:text>
</xsl:text>
    </xsl:variable>

    <xsl:template match="/">
        <xsl:choose>
            <xsl:when test="0">
                <xsl:text>Préparation des fichiers grec :</xsl:text>
                <xsl:value-of select="$newline"/>
                <xsl:for-each select="$files">
                    <xsl:variable name="file"
                        select="substring-before(replace(document-uri(.), '.*/', ''), '.xml')"/>
                    <xsl:text>  </xsl:text>
                    <xsl:value-of select="$file"/>
                    <xsl:text>: </xsl:text>

                    <!-- Pre-process greek files -->
                    <xsl:variable name="greek_filename">
                        <xsl:text>../tei/greek/?select=greek_</xsl:text>
                        <xsl:value-of select="$file"/>
                        <xsl:text>.xml</xsl:text>
                    </xsl:variable>
                    <xsl:variable name="greek_file" select="collection($greek_filename)"/>

                    <xsl:result-document href="byPage/greek/greek_{$file}.xml" method="xml"
                        indent="yes" omit-xml-declaration="yes">
                        <xsl:apply-templates mode="splitByAnchor" select="$greek_file"/>
                    </xsl:result-document>
                    <xsl:text>  greek spliting ok </xsl:text>

                    <!-- END -->
                    <xsl:text>... DONE</xsl:text>
                    <xsl:value-of select="$newline"/>
                </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>Préparation des fichiers grec :</xsl:text>
                <xsl:value-of select="$newline"/>
                <xsl:text>  DÉSACTIVÉE: Pour activer, modifier test='0' dans corpusProcessing.xsl</xsl:text>
                <xsl:value-of select="$newline"/>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:text>Création de la page d'accueil :</xsl:text>
        <xsl:value-of select="$newline"/>
        <xsl:apply-templates select="//div[@xml:id = 'home']" mode="home"/>

        <xsl:text>Création de la note éditoriale :</xsl:text>
        <xsl:value-of select="$newline"/>
        <xsl:apply-templates select="//div[@xml:id = 'edito']" mode="edito"/>

        <xsl:text>Transformation des pièces :</xsl:text>
        <xsl:value-of select="$newline"/>

        <xsl:for-each select="//tei:TEI[@type = 'play']">
            <xsl:variable name="play" select="@xml:id"/>
            <xsl:text>  </xsl:text>
            <xsl:value-of select="$play"/>
            <xsl:text>: </xsl:text>
            <!-- NEXT and PREVIOUS play -->
            <xsl:variable name="previous">
                <!--<xsl:value-of select="//tei:TEI[@xml:id = $play and @type='play']/preceding-sibling::tei:TEI[@type = 'play'][1]/@xml:id"
                />-->
                <xsl:value-of select="./preceding-sibling::tei:TEI[@type = 'play'][1]/@xml:id"/>
            </xsl:variable>
            <xsl:text>[PREV: </xsl:text>
            <xsl:value-of select="$previous"/>
            <xsl:text>]</xsl:text>

            <xsl:variable name="next">
                <xsl:value-of select="./following-sibling::tei:TEI[@type = 'play'][1]/@xml:id"/>
            </xsl:variable>
            <xsl:text>[NEXT: </xsl:text>
            <xsl:value-of select="$next"/>
            <xsl:text>]</xsl:text>

            <!-- Create view pages -->
            <xsl:result-document href="../views/html/{$play}.html" method="xhtml" indent="yes"
                omit-xml-declaration="yes">
                <xsl:apply-templates mode="view" select=".">
                    <xsl:with-param name="play" select="$play"/>
                    <xsl:with-param name="previous" select="$previous"/>
                    <xsl:with-param name="next" select="$next"/>
                </xsl:apply-templates>
            </xsl:result-document>
            <xsl:text> view ok</xsl:text>

            <!-- Create index of person pages -->
            <xsl:result-document href="../views/html/person_{$play}.html" method="xhtml"
                indent="yes" omit-xml-declaration="yes">
                <xsl:apply-templates mode="person" select=".">
                    <xsl:with-param name="play" select="$play"/>
                </xsl:apply-templates>
            </xsl:result-document>
            <xsl:text>, person ok</xsl:text>

            <!-- Create parallel view -->
            <xsl:result-document href="../views/html/parallel_{$play}.html" method="xhtml"
                indent="yes" omit-xml-declaration="yes">
                <xsl:apply-templates mode="parallel" select=".">
                    <xsl:with-param name="play" select="$play"/>
                </xsl:apply-templates>
            </xsl:result-document>
            <xsl:text>, parallel ok</xsl:text>

            <!-- END -->
            <xsl:text>... DONE</xsl:text>
            <xsl:value-of select="$newline"/>
        </xsl:for-each>

        <xsl:value-of select="$newline"/>
        <xsl:text>FIN</xsl:text>
        <xsl:value-of select="$newline"/>
    </xsl:template>

    <xsl:template match="//tei:div[@xml:id = 'home']" mode="home">
        <xsl:result-document href="../views/html/index.html" method="xhtml" indent="yes">
            <div id="main" class="col-10 offset-1">
                <xsl:apply-templates mode="editorial"/>
            </div>
        </xsl:result-document>
    </xsl:template>

    <xsl:template match="//tei:div[@xml:id = 'edito']" mode="edito">
        <xsl:result-document href="../views/html/edito.html" method="xhtml" indent="yes">
            <div id="main" class="col-10 offset-1">
                <xsl:apply-templates mode="editorial"/>
            </div>
        </xsl:result-document>
    </xsl:template>

    <xsl:template match="tei:head[@rend]" mode="editorial">
        <xsl:element name="{@rend}">
            <xsl:apply-templates mode="editorial"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="tei:head" mode="editorial">
        <h3>
            <xsl:apply-templates mode="editorial"/>
        </h3>
    </xsl:template>

    <xsl:template match="tei:p" mode="editorial">
        <p>
            <xsl:apply-templates mode="editorial"/>
        </p>
    </xsl:template>

    <xsl:template match="tei:hi" mode="editorial">
        <xsl:choose>
            <xsl:when test="@rend = 'italic'">
                <i>
                    <xsl:apply-templates mode="editorial"/>
                </i>
            </xsl:when>
            <xsl:otherwise>
                <mark>
                    <xsl:apply-templates mode="editorial"/>
                </mark>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:ref" mode="editorial">
        <a class="fa fa-link" href="{@target}">
            <xsl:apply-templates mode="editorial"/>
        </a>
    </xsl:template>

    <xsl:template match="tei:gi | tei:tag" mode="editorial">
        <code>&lt;<xsl:value-of select="."/>&gt;</code>
    </xsl:template>

    <xsl:template match="tei:att" mode="editorial">
        <code>@<xsl:value-of select="."/></code>
    </xsl:template>

    <xsl:template match="tei:code" mode="editorial">
        <code>
            <xsl:apply-templates mode="editorial"/>
        </code>
    </xsl:template>

    <xsl:template match="tei:lb" mode="editorial">
        <br/>
    </xsl:template>

    <xsl:template mode="TOC" match="head">
        <h2>
            <xsl:value-of select="."/>
        </h2>
    </xsl:template>

    <xsl:template mode="test" match="test">
        <xsl:for-each select="section">
            <xsl:result-document href="foo.html">
                <!-- add instructions to generate document content here -->
                <xsl:apply-templates select="collection('../tei/?select=*.xml')"/>
            </xsl:result-document>
        </xsl:for-each>

    </xsl:template>
</xsl:stylesheet>

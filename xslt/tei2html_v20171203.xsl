<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    <xsl:output method="xhtml" omit-xml-declaration="yes"/>
    <xsl:output indent="yes"/>
    <xsl:strip-space elements="*"/>
    <!--    sortie xml pour obtenir balises meta img et link auto-fermantes -->
    <!-- septembre-octobre 2017 - Nathalie Arlin - feuille de transformation dans le cadre du projet l'Aristophane de Lobineau-->
    <!-- 17 novembre 2017 - AnneGF - Corrections mineures -->
    <!-- 03 décembre 2017 - AnneGF - Adaptation pour framework Twig -->
    <!-- 05 décembre 2017 - Arnaud - tentative de modif pour ne pas avoir à générer du twig -->

    <xsl:template match="/">
        <style>
            .row {
                border: red 1px solid;
            }
            .col {
                border: green 1px solid;
            }</style>
        <!-- feuille pour transformer fichier lobineau-tei en html -->
        <div class="row">
            <div class="col col-12">
                <div class="row" name="top_banner">
                    <div class="col col-2" style="font-size:small; color:lightgrey;">
                        <i>Mise à jour : <xsl:value-of
                                select="format-date(current-date(), '[D01]/[M01]/[Y0001]')"/></i>
                    </div>
                    <div class="col col-1 offset-9">
                        <xsl:element name="a">
                            <xsl:attribute name="href">tei/<xsl:value-of
                                    select="tokenize(document-uri(.), '/')[last()]"
                                /></xsl:attribute>
                            <xsl:attribute name="id">tei_link</xsl:attribute>
                            <xsl:attribute name="download"/>
                            <img src="img/logo_TEI.svg" title="Voir la source TEI"
                                alt="Voir la source TEI" width="30px"
                                style="vertical-align: middle;"/>
                        </xsl:element>
                    </div>
                </div>
                <div class="row page">
                    <xsl:apply-templates/>
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="teiHeader"/>
    <!--on ne veut pas le contenu du header-->

    <!-- Regroupement par pages -->
    <!--<xsl:template match="front">
        <xsl:for-each-group select="*" group-starting-with="pb">
            <div class="page" style="border: 1px solid black;">
                <xsl:apply-templates select="current-group()"/>
            </div>
        </xsl:for-each-group>
    </xsl:template>-->
    <!-- END Regroupement par pages -->


    <!--  affichage et liens vers les fac-similes - Rem. On enleve le # dans nom de fichier -->
    <xsl:template match="pb">
        <xsl:text disable-output-escaping="yes">&lt;/div>&lt;/div>&lt;div class="row page"></xsl:text>
        <hr style="clear:both"/>
        <div class="col col-2 order-1 imagette">
            <xsl:variable name="facs" select="substring-after(@facs, '#')"/>
            <span onclick='modaleHandler.createImageModale("{$facs}")'>
                <xsl:element name="img">
                    <xsl:attribute name="class">thumbnail</xsl:attribute>
                    <xsl:attribute name="title">voir la page</xsl:attribute>
                    <xsl:attribute name="src">facs/thumbnails/<xsl:value-of select="$facs"
                        /></xsl:attribute>
                    <xsl:attribute name="alt">fac-similé <xsl:value-of select="$facs"
                        /></xsl:attribute>
                </xsl:element>
            </span>
        </div>
        <xsl:text disable-output-escaping="yes">&lt;div class="col col-9 order-3 page"></xsl:text>
    </xsl:template>

    <xsl:template match="div[@type = 'didascalie']">
        <div id="didascalie">
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="fw[@type = 'pageNum']">
        <div class="pagination">
            <span class="pagination">
                <xsl:if test="@rend">
                    <xsl:attribute name="class">
                        <xsl:value-of select="@rend"/>
                    </xsl:attribute>
                </xsl:if>
                <xsl:apply-templates/>
            </span>
        </div>
    </xsl:template>

    <xsl:template match="fw[@type = 'catch']">
        <span class="reclame">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="foliotation">
        <span class="foliotation">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="metamark">
        <p>
            <span class="metamark">
                <xsl:apply-templates/>
            </span>
        </p>
    </xsl:template>

    <xsl:template match="head[not(@*)] | head[@type = 'main']">
        <h1>
            <xsl:apply-templates/>
        </h1>
    </xsl:template>

    <xsl:template match="head[@type = 'sub']">
        <h2>
            <xsl:apply-templates/>
        </h2>
    </xsl:template>

    <xsl:template match="p">
        <!--<div class="paragraph">-->
        <p>
            <xsl:apply-templates/>
        </p>
        <!--</div>-->
    </xsl:template>

    <xsl:template match="lb">
        <br/>
        <xsl:apply-templates/>

    </xsl:template>

    <xsl:template match="speaker">
        <span class="speaker">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="didascalie">
        <span class="didascalie">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <!-- Notes et références aux notes -->
    <!-- TODO :
        - permettre des notes dans des notes
        - affichage sélectif selon la hand
        - ne pas activer le lien si la hand de la référence n'est pas active
    -->
    <xsl:template match="ref[@target]">
        <a>
            <xsl:attribute name="href">
                <xsl:text>#note_</xsl:text>
                <xsl:value-of select="substring-after(@target, '#')"/>
            </xsl:attribute>
            <xsl:attribute name="id">
                <xsl:text>note-ref_</xsl:text>
                <xsl:value-of select="substring-after(@target, '#')"/>
            </xsl:attribute>
            <xsl:choose>
                <xsl:when test="@rend = 'sup'">
                    <sup>
                        <xsl:apply-templates/>
                    </sup>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates/>
                </xsl:otherwise>
            </xsl:choose>
        </a>
        <xsl:if test=".//name">
            <xsl:message>WARNING: In <xsl:value-of select="base-uri()"/> - ref node contains a name
                node. It could cause html display incoherence due to the fact that both are
                transformed into html hyperlink</xsl:message>
        </xsl:if>
    </xsl:template>

    <xsl:template match="note">
        <!--<div class="note">-->
        <xsl:variable name="hand">
            <xsl:value-of select="substring-after(@hand, '#')"/>
        </xsl:variable>
        <xsl:text disable-output-escaping="yes">&lt;/div></xsl:text>
        <div class="col col-2 order-2">
            <p>
            <xsl:if test="@hand">
                <xsl:attribute name="data-author">
                    <xsl:choose>
                        <xsl:when test="name(/TEI/teiHeader//*[@xml:id = $hand]/..) = 'respStmt'">
                            <xsl:text>editeur </xsl:text>
                            <xsl:value-of select="$hand"/>
                        </xsl:when>
                        <xsl:when test="name(/TEI/teiHeader//*[@xml:id = $hand]/..) = 'handDesc'">
                            <xsl:text>manuscript </xsl:text>
                            <xsl:value-of select="$hand"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>other </xsl:text>
                            <xsl:value-of select="$hand"/>
                            <xsl:message>WARNING: In <xsl:value-of select="base-uri()"/> - Value of
                                @hand used in note (id: <xsl:value-of select="@xml:id"/>) declared
                                nor in respStmt, nor in handDesc: <xsl:value-of select="@hand"
                                /></xsl:message>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:attribute>
            </xsl:if>
            <span class="note">
                <xsl:if test="$hand = '#pencil'">
                    <xsl:attribute name="data-hand">
                        <xsl:text>crayon</xsl:text>
                    </xsl:attribute>
                </xsl:if>
                <xsl:if test="@type = 'editor'">
                    <xsl:attribute name="type">
                        <xsl:text>editeur</xsl:text>
                    </xsl:attribute>
                </xsl:if>
                <xsl:attribute name="id">
                    <xsl:text>note_</xsl:text>
                    <xsl:value-of select="@xml:id"/>
                </xsl:attribute>
                <xsl:apply-templates/>
                <a>
                    <xsl:attribute name="href">
                        <xsl:text>#note-ref_</xsl:text>
                        <xsl:value-of select="@xml:id"/>
                    </xsl:attribute> &#9658; </a>
            </span>
        </p>
        </div>
        <xsl:text disable-output-escaping="yes">&lt;div class="col col-9 order-3"></xsl:text>
        <!--</div>-->
    </xsl:template>

    <!--Personnages et leurs attributs -->
    <xsl:template match="name">
        <xsl:variable name="id" select="substring-after(@ref, '#')"/>
        <xsl:if test="@ref">
            <span class="personnage">
                <a>
                    <xsl:attribute name="href">
                        <xsl:text>#pers_</xsl:text>
                        <xsl:value-of select="$id"/>
                    </xsl:attribute>
                    <xsl:attribute name="data-id">
                        <xsl:value-of select="$id"/>
                    </xsl:attribute>
                    <xsl:attribute name="data-role">
                        <xsl:value-of select="//person[@xml:id = $id]/@role"/>
                    </xsl:attribute>
                    <xsl:apply-templates/>
                </a>
            </span>
        </xsl:if>

        <!-- Lieux -->
        <xsl:if test="@type = 'place'">
            <span class="lieu">
                <xsl:apply-templates/>
            </span>
        </xsl:if>

        <!-- Oeuvres -->
        <xsl:if test="@type = 'oeuvre'">
            <span class="oeuvre">
                <xsl:apply-templates/>
            </span>
        </xsl:if>
    </xsl:template>

    <!-- Personnnages de la pièce à présenter en liste sans puce -->
    <xsl:template match="castList">
        <ul>
            <xsl:for-each select="castItem">
                <li class="actor">
                    <span class="acteur">
                        <xsl:value-of select="."/>
                    </span>
                </li>

            </xsl:for-each>
        </ul>
    </xsl:template>

    <xsl:template match="seg[@type = 'realia']">
        <span class="realia">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="hi[@rend = 'line-through']">
        <del>
            <xsl:apply-templates/>
        </del>
    </xsl:template>

    <xsl:template match="citation">
        <q>
            <xsl:apply-templates/>
        </q>
    </xsl:template>

    <xsl:template match="hi[@rend = 'underline']">
        <u>
            <xsl:apply-templates/>
        </u>
    </xsl:template>

    <xsl:template match="stage">
        <span class="stage">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="add">
        <span class="ajout">
            <xsl:choose>
                <xsl:when test="@hand">
                    <span>
                        <xsl:attribute name="class">
                            <xsl:value-of select="@hand"/>
                        </xsl:attribute>
                        <xsl:if test="@rend = 'sup'">
                            <sup>
                                <xsl:apply-templates/>
                            </sup>
                        </xsl:if>
                        <xsl:if test="not(@rend = 'sup')">
                            <xsl:apply-templates/>
                        </xsl:if>
                    </span>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:if test="@rend = 'sup'">
                        <sup>
                            <xsl:apply-templates/>
                        </sup>
                    </xsl:if>
                </xsl:otherwise>
            </xsl:choose>
        </span>
    </xsl:template>

    <xsl:template match="date">
        <span class="date">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="unclear">
        <span title="texte peu lisible. Il s'agit ici d'une proposition de transcription"
            class="unclear">
            <xsl:apply-templates/>
            <i>[?]</i>
        </span>
    </xsl:template>


    <xsl:template match="gap">
        <span title="texte illisible" class="gap">[illisible]</span>
    </xsl:template>

    <xsl:template match="sic">
        <xsl:apply-templates/>
        <i> [sic]</i>
    </xsl:template>


</xsl:stylesheet>

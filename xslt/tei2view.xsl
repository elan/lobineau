<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mon-document [
    <!ENTITY times "&#215;">
    <!ENTITY carriage_return "&#8617;" >
    <!ENTITY non_breakable_space "&#160;" >
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="2.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">

    <!--<xsl:output method="xhtml" omit-xml-declaration="yes" indent="yes"/>-->
    <xsl:strip-space elements="*"/>


    <xsl:variable name="default_debug_mode">0</xsl:variable>
    <xsl:variable name="default_view_editor_notes">0</xsl:variable>

    <xsl:key name="txt-by-pb" match="node()"
        use="generate-id(preceding-sibling::pb[position() = 1])"/>

    <xsl:template match="tei:TEI" mode="view">
        <xsl:param name="play"/>
        <xsl:param name="previous"/>
        <xsl:param name="next"/>
        <xsl:param name="view_editor_notes" select="$default_view_editor_notes"/>
        <xsl:param name="debug" select="$default_debug_mode"/>

        <xsl:variable name="filename">
            <xsl:value-of select="$play"/>
            <xsl:text>.xml</xsl:text>
        </xsl:variable>
        <!--<style about="to_move_when_ready">
        </style>-->
        <xsl:if test="$debug = 1">
            <style>
                .row {
                    border: red 1px solid;
                }
                .col {
                    border: green 1px solid;
                }
                .clearfix::before {
                    content: "";
                    clear: both;
                    display: table;
                }
                .imagette {
                    background: yellow;
                }
                .notes {
                    background: orange;
                }
                .transcription {
                    background: lightgrey;
                }</style>
        </xsl:if>
        <xsl:if test="$default_view_editor_notes = 1">
            <style>
                
            </style>
        </xsl:if>

        <div class="row content">
            <div class="col col-12">
                <div class="row top_banner">
                    <div class="col col-2 text-left" style="font-size:small;">
                        <xsl:if test="string-length($previous) > 0">
                            <a class="btn btn-light"
                                href="index.php?page={$previous}&amp;play={$previous}">
                                <i class="fa fa-chevron-left"/>
                                <i>
                                    <xsl:value-of
                                        select="./preceding-sibling::tei:TEI[@xml:id = $previous]//tei:title[@type = 'ouvrage']"
                                    />
                                </i>
                            </a>
                        </xsl:if>
                    </div>
                    <div class="col col-8 text-center" style="font-size:small; color:lightgrey;">
                        <xsl:element name="a">
                            <xsl:attribute name="href">tei/<xsl:value-of select="$filename"
                                /></xsl:attribute>
                            <xsl:attribute name="id">tei_link</xsl:attribute>
                            <xsl:attribute name="download"/>
                            <img src="img/logo_TEI.svg" title="Voir la source TEI"
                                alt="Voir la source TEI" width="30px"
                                style="vertical-align: middle;"/>
                        </xsl:element>
                        <i>Mise à jour : <xsl:value-of
                                select="format-date(current-date(), '[D01]/[M01]/[Y0001]')"/></i>
                    </div>
                    <div class="col col-2 text-right" style="font-size:small;">
                        <xsl:if test="string-length($next) > 0">
                            <a class="btn btn-light" href="index.php?page={$next}&amp;play={$next}">
                                <i>
                                    <xsl:value-of
                                        select="./following-sibling::tei:TEI[@xml:id = $next]//tei:title[@type = 'ouvrage']"
                                    />
                                </i>
                                <i class="fa fa-chevron-right"/>
                            </a>
                        </xsl:if>
                    </div>
                </div>
                <xsl:for-each select=".//tei:pb">
                    <xsl:variable name="this_pb">
                        <xsl:choose>
                            <xsl:when test="exists(following-sibling::tei:fw)">
                                <xsl:value-of select="following-sibling::tei:fw[1]"/>
                            </xsl:when>
                            <xsl:when test="exists(following-sibling::tei:sp[1]/tei:fw)">
                                <xsl:value-of select="following-sibling::tei:sp[1]/tei:fw[1]"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:message>
                                    <xsl:text>WARNING: In </xsl:text>
                                    <xsl:value-of select="base-uri()"/>
                                    <xsl:text>: pb not followed by fw</xsl:text>
                                </xsl:message>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
                    <div class="row">
                        <div class="col col-1 imagette order-1">
                            <xsl:apply-templates select="." mode="do"/>
                        </div>
                        <div class="col col-2 order-2 notes_and_info">
                            <xsl:for-each select="key('txt-by-pb', generate-id())">
                                <xsl:if test="./ancestor::tei:TEI[@xml:id = $play]">
                                    <xsl:apply-templates select="tei:fw[@type = 'pageNum']" mode="do"/>
                                    <xsl:if test="name(.) = 'fw' and @type = 'pageNum'">
                                        <xsl:apply-templates select="." mode="do"/>
                                    </xsl:if>
                                </xsl:if>
                            </xsl:for-each>
                            <xsl:for-each select="key('txt-by-pb', generate-id())">
                                <xsl:if test="./ancestor::tei:TEI[@xml:id = $play]">
                                    <xsl:apply-templates select="tei:note" mode="do"/>
                                    <xsl:if test="name(.) = 'note'">
                                        <xsl:apply-templates select="." mode="do"/>
                                    </xsl:if>
                                    <xsl:if
                                        test="//tei:TEI[@xml:id = $play]//tei:front/descendant::tei:pb[last()]/following-sibling::tei:fw[1] = $this_pb and position() = last()">
                                        <xsl:for-each
                                            select="//tei:TEI[@xml:id = $play]//tei:body//tei:pb[1]/preceding::tei:note[exists(ancestor::tei:body)]">
                                            <xsl:if test="./ancestor::tei:TEI[@xml:id = $play]">
                                                <xsl:apply-templates select="." mode="do"/>
                                            </xsl:if>
                                        </xsl:for-each>
                                    </xsl:if>
                                </xsl:if>
                            </xsl:for-each>
                        </div>
                        <div class="col col-9 order-3 transcription">
                            <xsl:for-each select="key('txt-by-pb', generate-id())">
                                <xsl:if test="./ancestor::tei:TEI[@xml:id = $play]">
                                    <xsl:apply-templates select="."/>
                                    <xsl:if
                                        test="//tei:TEI[@xml:id = $play]//tei:front/descendant::tei:pb[last()]/following-sibling::tei:fw[1] = $this_pb and position() = last()">
                                        <xsl:for-each
                                            select="//tei:TEI[@xml:id = $play]//tei:body//tei:pb[1]/preceding::*[exists(ancestor::tei:body) and name() != 'note' and name() != 'ref' and name() != 'name' and name() != 'seg' and name() != 'div'] except //tei:TEI[@xml:id = $play]//tei:body//tei:pb[1]/preceding-sibling::*">
                                            <xsl:if test="./ancestor::tei:TEI[@xml:id = $play]">
                                                <xsl:apply-templates
                                                  select=". except .[parent::tei:note]"/>
                                            </xsl:if>
                                        </xsl:for-each>
                                    </xsl:if>
                                </xsl:if>
                            </xsl:for-each>
                        </div>
                    </div>
                </xsl:for-each>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="teiHeader"/>
    <!--on ne veut pas le contenu du header-->

    <!--<xsl:template match="div[not(@type = 'didascalie')]">
        <xsl:apply-templates/>
    </xsl:template>-->

    <xsl:template match="pb"/>

    <xsl:template match="pb" mode="do">
        <xsl:variable name="facs" select="substring-after(@facs, '#')"/>
        <span onclick='modaleHandler.createImageModale("{$facs}")'>
            <xsl:element name="img">
                <xsl:attribute name="class">thumbnail</xsl:attribute>
                <xsl:attribute name="title">voir la page</xsl:attribute>
                <xsl:attribute name="src">facs/thumbnails/<xsl:value-of select="$facs"
                    /></xsl:attribute>
                <xsl:attribute name="alt">fac-similé <xsl:value-of select="$facs"/></xsl:attribute>
            </xsl:element>
        </span>
    </xsl:template>

    <xsl:template match="note">
        <xsl:variable name="hand">
            <xsl:value-of select="substring-after(@hand, '#')"/>
        </xsl:variable>
        <xsl:variable name="is_editorial">
            <xsl:choose>
                <xsl:when
                    test="name(ancestor::tei:TEI/tei:teiHeader//*[@xml:id = $hand]/..) = 'respStmt'">
                    <xsl:value-of select="true()"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="false()"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="editor">
            <xsl:value-of select="ancestor::tei:TEI/tei:teiHeader//*[@xml:id = $hand]"/>
        </xsl:variable>
        <xsl:if test="$is_editorial = true()">
            <div class="modal" id="{@xml:id}" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Note éditoriale de
                                    <xsl:value-of select="$editor"/></h5>
                            <button type="button" class="close" data-dismiss="modal"
                                aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <xsl:apply-templates/>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"
                                >Fermer</button>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:template match="note[contains(@rend, 'inline')]">
        <xsl:choose>
            <xsl:when test=".[contains(@rend, 'sep-top-line')]">
                <hr class="note-inline-sep"/>
            </xsl:when>
            <xsl:otherwise/>
        </xsl:choose>
        <xsl:for-each select="tei:p">
            <p class="note-inline">
                <xsl:apply-templates/>
            </p>
        </xsl:for-each>
        <xsl:choose>
            <xsl:when test=".[contains(@rend, 'sep-bottom-line')]">
                <hr class="note-inline-sep"/>
            </xsl:when>
            <xsl:otherwise/>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="note[contains(@rend, 'inline')]" mode="do"/>

    <xsl:template match="tei:note" mode="do">
        <xsl:variable name="hand">
            <xsl:value-of select="substring-after(@hand, '#')"/>
        </xsl:variable>
        <xsl:variable name="is_editorial">
            <xsl:choose>
                <xsl:when
                    test="name(ancestor::tei:TEI/tei:teiHeader//*[@xml:id = $hand]/..) = 'respStmt'">
                    <xsl:value-of select="true()"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="false()"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:if test="$is_editorial = false()">
            <p>
                <xsl:if test="@hand">
                    <xsl:attribute name="class">
                        <xsl:text>note </xsl:text>
                        <xsl:choose>
                            <xsl:when
                                test="name(ancestor::tei:TEI/tei:teiHeader//*[@xml:id = $hand]/..) = 'respStmt'">
                                <xsl:text>editor </xsl:text>
                                <xsl:value-of select="$hand"/>
                            </xsl:when>
                            <xsl:when
                                test="name(ancestor::tei:TEI/tei:teiHeader//*[@xml:id = $hand]/..) = 'handDesc'">
                                <xsl:text>manuscript </xsl:text>
                                <xsl:value-of select="$hand"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:text>unknown_resp</xsl:text>
                                <xsl:value-of select="$hand"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>
                </xsl:if>
                <xsl:if
                    test="not(name(ancestor::tei:TEI/teiHeader//*[@xml:id = $hand]/..) = 'respStmt') and not(name(ancestor::tei:TEI/teiHeader//*[@xml:id = $hand]/..) = 'handDesc')">
                    <xsl:attribute name="title">
                        <xsl:text>Rem : La valeur de l'attribut @hand '</xsl:text>
                        <xsl:value-of select="@hand"/>
                        <xsl:text>' pour cette note (id: </xsl:text>
                        <xsl:value-of select="@xml:id"/>
                        <xsl:text>) ne correspond ni à un·e éditeur, ni à un·e  auteur</xsl:text>
                    </xsl:attribute>
                    <xsl:message>
                        <xsl:text>WARNING: In </xsl:text>
                        <xsl:value-of select="base-uri()"/>
                        <xsl:text>- Value of @hand used in note (id:</xsl:text>
                        <xsl:value-of select="@xml:id"/>
                        <xsl:text>) declared nor in respStmt, nor in handDesc:</xsl:text>
                        <xsl:value-of select="@hand"/>
                    </xsl:message>
                </xsl:if>
                <xsl:if test="not(@hand)">
                    <xsl:attribute name="class">
                        <xsl:text>note unknown_resp</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="title">
                        <xsl:text>Rem : pas de valeur pour l'attribut @hand pour cette note (id: </xsl:text>
                        <xsl:value-of select="@xml:id"/>
                        <xsl:text>). </xsl:text>
                    </xsl:attribute>
                    <xsl:message>
                        <xsl:text>WARNING: In </xsl:text>
                        <xsl:value-of select="base-uri()"/>
                        <xsl:text> - No value for @hand in note (id: </xsl:text>
                        <xsl:value-of select="@xml:id"/>
                        <xsl:text>). </xsl:text>
                    </xsl:message>
                </xsl:if>
                <span class="thenote">
                    <xsl:if test="$hand = '#pencil'">
                        <xsl:attribute name="data-hand">
                            <xsl:text>crayon</xsl:text>
                        </xsl:attribute>
                    </xsl:if>
                    <xsl:if test="@type = 'editor'">
                        <xsl:attribute name="type">
                            <xsl:text>editeur</xsl:text>
                        </xsl:attribute>
                    </xsl:if>
                    <xsl:attribute name="id">
                        <xsl:text>note_</xsl:text>
                        <xsl:value-of select="@xml:id"/>
                    </xsl:attribute>
                    <xsl:apply-templates/>
                    <xsl:if test="@xml:id">
                        <a class="goto">
                            <xsl:attribute name="href">
                                <xsl:text>#note-ref_</xsl:text>
                                <xsl:value-of select="@xml:id"/>
                            </xsl:attribute>
                            <xsl:text>&#9658;</xsl:text>
                        </a>
                    </xsl:if>
                </span>
            </p>
        </xsl:if>
    </xsl:template>

    <xsl:template match="div[@type = 'didascalie']">
        <p id="didascalie">
            <xsl:apply-templates/>
        </p>
    </xsl:template>

    <xsl:template match="fw[@type = 'pageNum']"/>

    <xsl:template match="fw[@type = 'pageNum']" mode="do">
        <p class="pagenum">
            <xsl:if test="@rend">
                <xsl:attribute name="data-rend">
                    <xsl:value-of select="@rend"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:apply-templates/>
        </p>
    </xsl:template>

    <xsl:template match="fw[@type = 'catch']">
        <span class="reclame">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="fw[@type = 'folioNum']">
        <span class="foliotation align-{@rend}">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="metamark">
        <p>
            <span class="metamark">
                <xsl:apply-templates/>
            </span>
        </p>
    </xsl:template>

    <xsl:template match="head[not(@*)] | head[@type = 'main']">
        <h1>
            <xsl:apply-templates/>
        </h1>
    </xsl:template>

    <xsl:template match="head[@type = 'sub']">
        <h2>
            <xsl:apply-templates/>
        </h2>
    </xsl:template>

    <xsl:template match="p">
        <!--<div class="paragraph">-->
        <p>
            <xsl:apply-templates/>
        </p>
        <!--</div>-->
    </xsl:template>

    <xsl:template match="lb">
        <br/>
        <xsl:apply-templates/>

    </xsl:template>

    <xsl:template match="speaker">
        <span class="speaker">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="didascalie">
        <span class="didascalie">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <!-- Notes et références aux notes -->
    <!-- TODO :
        - permettre des notes dans des notes
        - affichage sélectif selon la hand
        - ne pas activer le lien si la hand de la référence n'est pas active
    -->
    <xsl:template match="ref[@target]">
        <xsl:variable name="note_id">
            <xsl:value-of select="substring-after(@target, '#')"/>
        </xsl:variable>
        <xsl:variable name="hand">
            <xsl:value-of
                select="substring-after(ancestor::tei:TEI//tei:note[@xml:id = $note_id]/@hand, '#')"
            />
        </xsl:variable>
        <xsl:variable name="is_editorial">
            <xsl:choose>
                <xsl:when
                    test="name(ancestor::tei:TEI/tei:teiHeader//*[@xml:id = $hand]/..) = 'respStmt'">
                    <xsl:value-of select="true()"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="false()"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="$is_editorial = true()">
                <xsl:variable name="output" xml:space="default"><xsl:apply-templates/>&lt;sup
                    class="note-call" data-toggle="modal" data-target="#<xsl:value-of
                        select="$note_id"/>"><xsl:value-of select="substring-after($note_id, 'n')"
                    />&lt;/sup></xsl:variable>
                <xsl:value-of select="normalize-space($output)" disable-output-escaping="yes"/>
            </xsl:when>
            <xsl:otherwise>
                <a>
                    <xsl:attribute name="href">
                        <xsl:text>#note_</xsl:text>
                        <xsl:value-of select="substring-after(@target, '#')"/>
                    </xsl:attribute>
                    <xsl:attribute name="id">
                        <xsl:text>note-ref_</xsl:text>
                        <xsl:value-of select="substring-after(@target, '#')"/>
                    </xsl:attribute>
                    <xsl:choose>
                        <xsl:when test="@rend = 'sup'">
                            <sup>
                                <xsl:apply-templates/>
                            </sup>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates/>
                        </xsl:otherwise>
                    </xsl:choose>
                </a>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:if test=".//name">
            <xsl:message>
                <xsl:text>WARNING: In </xsl:text>
                <xsl:value-of select="base-uri()"/>
                <xsl:text>- ref node contains a name node. It could cause html display incoherence due to the fact that both are transformed into html hyperlink</xsl:text>
            </xsl:message>
        </xsl:if>
    </xsl:template>

    <!--Personnages et leurs attributs -->
    <xsl:template match="name">
        <xsl:variable name="id" select="substring-after(@ref, '#')"/>
        <xsl:choose>
            <xsl:when test="@ref">
                <span class="personnage">
                    <xsl:attribute name="title">
                        <xsl:text>Personnage</xsl:text>
                    </xsl:attribute>
                    <a>
                        <xsl:attribute name="href">
                            <xsl:text>#pers_</xsl:text>
                            <xsl:value-of select="$id"/>
                        </xsl:attribute>
                        <xsl:attribute name="data-id">
                            <xsl:value-of select="$id"/>
                        </xsl:attribute>
                        <xsl:attribute name="data-role">
                            <xsl:value-of select="//person[@xml:id = $id]/@role"/>
                        </xsl:attribute>
                        <xsl:apply-templates/>
                    </a>
                </span>
            </xsl:when>
            <xsl:when test="@type = 'place'">
                <mark class="lieu">
                    <xsl:attribute name="title">
                        <xsl:text>Lieu</xsl:text>
                    </xsl:attribute>
                    <xsl:apply-templates/>
                </mark>
            </xsl:when>
            <xsl:when test="@type = 'oeuvre'">
                <mark class="oeuvre">
                    <xsl:attribute name="title">
                        <xsl:text>Œuvre</xsl:text>
                    </xsl:attribute>
                    <xsl:apply-templates/>
                </mark>
            </xsl:when>
            <xsl:when test="@type = 'evenement_historique'">
                <mark class="evenement_historique">
                    <xsl:attribute name="title">
                        <xsl:text>Événement historique</xsl:text>
                    </xsl:attribute>
                    <xsl:apply-templates/>
                </mark>
            </xsl:when>
            <xsl:otherwise>
                <mark class="name">
                    <xsl:attribute name="title">
                        <xsl:text>Nom de type non précisé</xsl:text>
                    </xsl:attribute>
                    <xsl:message>
                        <xsl:text>WARNING: In </xsl:text>
                        <xsl:value-of select="base-uri()"/>
                        <xsl:text>- One of @ref (name of a "personnage") or @type (other type of name) is needed for name: </xsl:text>
                        <xsl:value-of select="."/>
                    </xsl:message>
                    <xsl:apply-templates/>
                </mark>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- Personnnages de la pièce à présenter en liste sans puce -->
    <xsl:template match="castList">
        <ul>
            <xsl:for-each select="child::*">
                <xsl:choose>
                    <xsl:when test="name(.) = 'castItem'">
                        <li class="actor">
                            <span class="acteur">
                                <xsl:value-of select="."/>
                            </span>
                        </li>
                    </xsl:when>
                    <xsl:when test="name(.) = 'castGroup'">
                        <!-- Uncomment and add behavious if necessary - Just one case taken into account currently -->
                        <!--    <xsl:choose>
                            <xsl:when test="@rend= 'curly_brace_right'">-->
                        <li class="actor">
                            <table>
                                <tr>
                                    <td class="actor">
                                        <span class="acteur">
                                            <xsl:value-of select="tei:castItem[1]"/>
                                        </span>
                                    </td>
                                    <td rowspan="{count(child::castItem)}">
                                        <img src="img/curly_left.png"
                                            style="transform: rotate(180deg); width: 10px; height: {count(child::castItem)*3}ex;"
                                        />
                                    </td>
                                    <td rowspan="{count(child::castItem)}">
                                        <xsl:value-of select="tei:head"/>
                                    </td>
                                </tr>
                                <xsl:for-each select="tei:castItem[position() > 1]">
                                    <tr>
                                        <td class="actor">
                                            <span class="acteur">
                                                <xsl:value-of select="."/>
                                            </span>
                                        </td>
                                    </tr>
                                </xsl:for-each>
                            </table>
                        </li>
                        <!--</xsl:when>
                            <xsl:otherwise>
                                
                            </xsl:otherwise>
                        </xsl:choose>-->
                    </xsl:when>
                </xsl:choose>
            </xsl:for-each>
        </ul>
    </xsl:template>

    <xsl:template match="seg[@type = 'realia']">
        <span class="realia">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="hi[@rend = 'line-through']">
        <del>
            <xsl:apply-templates/>
        </del>
    </xsl:template>

    <xsl:template match="citation">
        <q>
            <xsl:apply-templates/>
        </q>
    </xsl:template>

    <xsl:template match="hi[@rend = 'underline']">
        <u>
            <xsl:apply-templates/>
        </u>
    </xsl:template>

    <xsl:template match="stage">
        <span class="stage">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="add">
        <span class="ajout">
            <xsl:choose>
                <xsl:when test="@hand">
                    <span>
                        <xsl:attribute name="class">
                            <xsl:value-of select="@hand"/>
                        </xsl:attribute>
                        <xsl:if test="@rend = 'sup'">
                            <sup>
                                <xsl:apply-templates/>
                            </sup>
                        </xsl:if>
                        <xsl:if test="not(@rend = 'sup')">
                            <xsl:apply-templates/>
                        </xsl:if>
                    </span>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:choose>
                        <xsl:when test="@rend = 'sup'">
                            <sup>
                                <xsl:apply-templates/>
                            </sup>
                        </xsl:when>
                        <xsl:when test="@rend = 'inline'">
                            <xsl:apply-templates/>
                        </xsl:when>
                        <xsl:otherwise>
                            <sup>
                                <xsl:apply-templates/>
                            </sup>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
        </span>
    </xsl:template>

    <xsl:template match="date">
        <span class="date">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="unclear">
        <span title="texte peu lisible. Il s'agit ici d'une proposition de transcription"
            class="unclear">
            <xsl:apply-templates/>
            <i>[?]</i>
        </span>
    </xsl:template>

    <xsl:template match="gap">
        <span title="texte illisible" class="gap">[illisible]</span>
    </xsl:template>

    <xsl:template match="sic">
        <xsl:apply-templates/>
        <i> [sic]</i>
    </xsl:template>

</xsl:stylesheet>
